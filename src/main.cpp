#include <AEEngine.h>

#include "gs_list.h"
#include "menu.h"
#include "singleplayer.h"
#include "multiplayer.h"
#include "gameserver.h"
#include "result.h"
#include "replay.h"
#include "defines.h"

void AddGameStates()
{
	AEGameStateMgrAdd(
		GS_MENU,
		GameStateMenuLoad,
		GameStateMenuInit,
		GameStateMenuUpdate,
		GameStateMenuDraw,
		GameStateMenuFree,
		GameStateMenuUnload);

	AEGameStateMgrAdd(
		GS_SINGLEPLAYER,
		GameStateSingleplayerLoad,
		GameStateSingleplayerInit,
		GameStateSingleplayerUpdate,
		GameStateSingleplayerDraw,
		GameStateSingleplayerFree,
		GameStateSingleplayerUnload);

	AEGameStateMgrAdd(
		GS_MULTIPLAYER,
		GameStateMultiplayerLoad,
		GameStateMultiplayerInit,
		GameStateMultiplayerUpdate,
		GameStateMultiplayerDraw,
		GameStateMultiplayerFree,
		GameStateMultiplayerUnload);

	AEGameStateMgrAdd(
		GS_SERVER,
		GameStateServerLoad,
		GameStateServerInit,
		GameStateServerUpdate,
		GameStateServerDraw,
		GameStateServerFree,
		GameStateServerUnload);


	AEGameStateMgrAdd(
		GS_RESULT,
		GameStateResultLoad,
		GameStateResultInit,
		GameStateResultUpdate,
		GameStateResultDraw,
		GameStateResultFree,
		GameStateResultUnload);

	AEGameStateMgrAdd(
		GS_REPLAY,
		GameStateReplayLoad,
		GameStateReplayInit,
		GameStateReplayUpdate,
		GameStateReplayDraw,
		GameStateReplayFree,
		GameStateReplayUnload);

	AEGameStateMgrInit(GS_MENU);
}
int main(void)
{
	// Hide Window
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	//ShowCursor(false);

	// Initialize the system
	AESysInit ("Asteroids", SCR_WIDTH, SCR_HEIGHT);

	// Add game states and init
	AddGameStates();
	// Game Loop
	AESysGameLoop();

	// free the system
	AESysExit();
}