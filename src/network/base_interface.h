#pragma once
#include <string>
#include <iostream>

#define BUFFERSIZE 512

using SOCKET = unsigned int;

struct sockaddr;
struct sockaddr_in;
struct Endpoint
{
	Endpoint(std::string, short p);
	Endpoint(const sockaddr_in & add);
	~Endpoint();

	std::string m_ip;
	short m_port;
	sockaddr_in* m_address;
};

class BaeInterface
{
protected:
	const short SERVER_PORT = 5000;
	// Winsock
	void init_lib();
	void shutdown_lib();

	SOCKET create_socket(const Endpoint& end);
	void shutdown_socket(SOCKET socket);

	void send(SOCKET sender, const Endpoint & end, const std::string & message);
	std::string receive(SOCKET receiver, const Endpoint & end, int& err);
	std::string receive_from_any(SOCKET receiver, Endpoint* & end, int& err);
			
	// Utils
	void set_size(std::string& str);
	void unset_size(std::string& str);
	void make_nonblocking(SOCKET sock);
	std::string get_local_ip();
	int check_error(bool continue_if_error = false);
};