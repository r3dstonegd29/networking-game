#include "base_interface.h"
#include <vector>

class Client : public BaeInterface
{
public:
	void Initialize();
	void Shutdown();

	void disconnect();
	void try_connection();
	void wait_for_start();
	std::vector<std::string> send_recv_input(std::string);
	void print();
	bool connected{ false };
	bool playing{ false };
	unsigned client_id;
private:
	// Input File
	void read_config();
	short local_port;
	std::string server_ip;
	std::string player_list;

	//Endpoints
	Endpoint* local{ nullptr };
	Endpoint* server{ nullptr };
	SOCKET socket;
};