#include "base_interface.h"
#include <vector>

class Server : public BaeInterface
{
public:
	void Initialize();
	void Update();
	void Shutdown();

private:
	bool playing{ false };
	std::vector<Endpoint*> players;
	std::vector<std::string> connections;
	std::vector<std::string> total_commands;
	unsigned counter = 0;
	SOCKET socket;
};