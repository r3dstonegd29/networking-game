#include "..\base_interface.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#pragma comment (lib, "ws2_32.lib")

Endpoint::Endpoint(std::string ip, short port)
	: m_ip(ip), m_port(port)
{
	m_address = new sockaddr_in;
	std::memset(m_address, 0, sizeof(sockaddr_in));
	m_address->sin_family = AF_INET;
	m_address->sin_port = htons(port);
	m_address->sin_addr.S_un.S_addr = inet_addr(ip.c_str());
}
Endpoint::Endpoint(const sockaddr_in & add)
	: m_ip(inet_ntoa(add.sin_addr))
	, m_port(add.sin_port)
{
	m_address = new sockaddr_in;
	std::memcpy(m_address, &add, sizeof(sockaddr_in));
}
Endpoint::~Endpoint()
{
	delete m_address;
}

void BaeInterface::init_lib()
{
	//initialize winsock
	WSADATA wsData;
	if (WSAStartup(MAKEWORD(2, 2), &wsData))
		check_error();
}

void BaeInterface::shutdown_lib()
{
	WSACleanup();
}

SOCKET BaeInterface::create_socket(const Endpoint & end)
{
	SOCKET s = socket(PF_INET, SOCK_DGRAM, 0);
	if(s == SOCKET_ERROR)
		check_error();

	if (::bind(s, reinterpret_cast<sockaddr *>(end.m_address), sizeof(sockaddr)) == SOCKET_ERROR)
		check_error();
	return s;
}

void BaeInterface::shutdown_socket(SOCKET socket)
{
	if (::shutdown(socket, SD_BOTH) == SOCKET_ERROR)
		check_error();
	closesocket(socket);
}

void BaeInterface::send(SOCKET sender, const Endpoint & end, const std::string & message)
{
	int flags = 0;
	const sockaddr * to = reinterpret_cast<sockaddr *>(end.m_address);
	const int to_length = sizeof(sockaddr);

	int bytesSent = ::sendto(sender, message.c_str(), BUFFERSIZE, flags, to, to_length);

	if (bytesSent == SOCKET_ERROR)
		check_error();
}

std::string BaeInterface::receive(SOCKET receiver, const Endpoint & end, int & err)
{
	char buffer[BUFFERSIZE] = { 0 };
	int flags = 0;
	sockaddr * from = reinterpret_cast<sockaddr *>(end.m_address);
	int from_length = sizeof(sockaddr);

	err = 0;
	int bytesRecv = ::recvfrom(receiver, buffer, BUFFERSIZE, flags, from, &from_length);
	if (bytesRecv == SOCKET_ERROR)
	{
		err = check_error(true);
		return std::string();
	}
	else
		return std::string(buffer);
}

std::string BaeInterface::receive_from_any(SOCKET receiver, Endpoint* & end, int & err)
{
	char buffer[BUFFERSIZE] = { 0 };
	int flags = 0;
	sockaddr_in SenderAddr;
	int SenderAddrSize = sizeof(SenderAddr);
	err = 0;
	int bytesRecv = ::recvfrom(receiver, buffer, BUFFERSIZE, flags, reinterpret_cast<sockaddr *>(&SenderAddr), &SenderAddrSize);
	if (bytesRecv == SOCKET_ERROR)
	{
		err = check_error(true);
		return std::string();
	}
	else
	{
		end = new Endpoint{ SenderAddr };
		return std::string(buffer, bytesRecv);
	}
}

void BaeInterface::set_size(std::string & str)
{
	std::string size = std::to_string(str.size());
	size += '|';
	str.insert(str.begin(), size.begin(), size.end());

}

void BaeInterface::unset_size(std::string & str)
{
	unsigned line = str.find('|');
	unsigned size = atoi(str.substr(0, line).c_str());
	str = str.substr(line+1, size);
}

void BaeInterface::make_nonblocking(SOCKET socket)
{
	u_long mode = 1;
	if(ioctlsocket(socket, FIONBIO, &mode) != NO_ERROR)
		check_error();
}
std::string BaeInterface::get_local_ip()
{
	return std::string(inet_ntoa(*(reinterpret_cast<in_addr*>(*gethostbyname("")->h_addr_list))));
}

int BaeInterface::check_error(bool continue_if_error)
{
	int error = WSAGetLastError();
	if ( !continue_if_error && error > 0)
	{
		std::cout << "winsock error: " << error << std::endl;
		abort();
	}
	return error;
}
