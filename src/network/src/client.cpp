#include "..\client.h"
#include <iostream>
#include <fstream>
#include <AEEngine.h>
#include <time.h>   

void Client::Initialize()
{
	init_lib();
	read_config();
	socket = create_socket(*local);
	make_nonblocking(socket);
}


void Client::Shutdown()
{
	disconnect();
	delete server;
	delete local;
	shutdown_socket(socket);
	shutdown_lib();
}

void Client::disconnect()
{
	if (connected)
	{
		std::string str = "disconnect|" + get_local_ip() + '|' + std::to_string(local_port);
		set_size(str);
		send(socket, *server, str);

		connected = false;
	}
	
}

void Client::try_connection()
{
	if (!connected)
	{
		int err;
		std::string str = receive(socket, *server, err);
		if (!str.empty())
		{
			unset_size(str);
			client_id = atoi(str.substr(str.find('P') + 1).c_str());
			connected = true;

			player_list = str.substr(str.find("Player"));
		}
		else
		{
			str = "connect|" + get_local_ip() + '|' + std::to_string(local_port);
			set_size(str);
			send(socket, *server, str);
		}
	}
}

void Client::wait_for_start()
{
	int err;
	std::string str = receive(socket, *server, err);
	if (!str.empty())
	{
		unset_size(str);
		if(str == "start") playing = true;
		else player_list = str.substr(str.find("Player"));

	}
	else if (AEInputKeyTriggered(VK_SPACE))
	{
		str = "start";
		set_size(str);
		send(socket, *server, str);
	}
}

std::vector<std::string> Client::send_recv_input(std::string str)
{
	set_size(str);
	send(socket, *server, str);

	int err;
	std::vector<std::string> commands;
	do
	{
		std::string str = receive(socket, *server, err);
		if (!str.empty())
		{
			unset_size(str);
			commands.push_back(str);
		}
	} while (err != 10035);
	return commands;
}

void Client::print()
{
	s8 strBuffer[1024];
	sprintf(strBuffer, player_list.c_str());
	AEGfxPrint(280, 260, -1, strBuffer);
}

void Client::read_config()
{
	auto parse = [](std::string& str)
	{
		auto first = str.find('(');
		auto second = str.find(')');
		str = str.substr(first + 1, second - first - 1);
	};
	std::ifstream file{ "config" };
	if (file.is_open())
	{
		bool open = file.is_open();
		std::string line;

		std::getline(file, line); parse(line);
		if (line == "local")
		{
			server_ip = get_local_ip();
			srand(static_cast<u32>(time(nullptr)));
			local_port = 5001 + rand() % 1000;
		}
		else
		{
			server_ip = line;
			std::getline(file, line); parse(line);
			local_port = static_cast<short>(atoi(line.c_str()));
		}

		
		file.close();

		server = new Endpoint{ server_ip, SERVER_PORT };
		local = new Endpoint{ get_local_ip(), local_port };
	}
	else
		check_error();
}
