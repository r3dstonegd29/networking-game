#include "server.h"
#include <AEEngine.h>
#include <fstream>

void Server::Initialize()
{
	init_lib();
	socket = create_socket(Endpoint{ get_local_ip(), SERVER_PORT });
	make_nonblocking(socket);
}

void Server::Update()
{
	if (playing)
	{
		int err;
		std::vector<std::string> commands;
		for (auto it = players.begin(); it!= players.end();)
		{
			bool erased = false;
			auto end = *it;
			std::string str;
			//do
			//{
				 str.clear();
				 str = receive(socket, *end, err);
				if (!str.empty())
				{
					unset_size(str);
					if (str.substr(0, 10) == "disconnect")
					{
						str = str.substr(11);
						for (unsigned i = 0; i < connections.size(); i++)
							if (str == connections[i])
							{
								//Delete player from the list
								connections.erase(std::find(connections.begin(), connections.end(), connections[i]));
								it = players.erase(std::find(players.begin(), players.end(), players[i]));
								erased = true;
								break;
							}
						if (players.size() == 0)
						{
							playing = false;
							return;
						}
							
					}
					else
					{
						commands.push_back(str);
						total_commands.push_back(str);
					}
					
				}
			//} while (err != 10035 || str.empty());
			if (!erased) it++;
		}
		for (Endpoint* end : players)
			for (auto s : commands)
			{
				set_size(s);
				send(socket, *end, s);
			}

		/*s8 strBuffer[1024];
		unsigned j = 0;
		for (unsigned i = counter; i < total_commands.size(); i++)
		{
			sprintf(strBuffer, total_commands[i].c_str());
			AEGfxPrint(0, 30 * j, -1, strBuffer);
			j++;

			if (j > 20)
			{
				counter++;
				break;
			}
		}*/
		
	}
	else // Lobby
	{
		int err;
		Endpoint * end;//not working
		std::string str = receive_from_any(socket, end, err);
		if (!str.empty())
		{
			unset_size(str);
			if (str.substr(0, 7) == "connect")
			{
				str = str.substr(8);
				for (std::string& c : connections)
					if (str == c) return;

				unsigned sep = str.find('|');
				std::string newip = str.substr(0, sep);
				short newport = atoi(str.substr(sep+1).c_str());

				players.push_back(new Endpoint{ newip, newport });
				connections.push_back(str);

				str = "connect-P" + std::to_string( players.size()-1);

				for (unsigned i = 0; i < players.size(); i++ )
				{
					str += "Player" + std::to_string(i + 1) + " " + connections[i] + '\n';
				}

				set_size(str);
				for(auto player : players)
					send(socket, *player, str);

			}
			else if (str.substr(0, 10) == "disconnect")
			{
				str = str.substr(11);
				for (unsigned i = 0; i < connections.size(); i++)
					if (str == connections[i])
					{
						//Delete player from the list
						connections.erase(std::find(connections.begin(), connections.end(), connections[i]));
						players.erase(std::find(players.begin(), players.end(), players[i]));
						//Send updated list
						std::string player_list;
						for (unsigned i = 0; i < players.size(); i++)
							player_list += "Player" + std::to_string(i + 1) + " " + connections[i] + '\n';

						set_size(player_list);
						for (auto player : players)
							send(socket, *player, player_list);
						break;
					}
					
			}

			if (str == "start")
			{
				set_size(str);
				for (Endpoint* end : players)
					send(socket, *end, str);
				playing = true;
			}
		}
	}
}

void Server::Shutdown()
{
	std::ofstream journal;
	journal.open("journal.txt");
	if (journal.is_open())
	{
		for (auto commands : total_commands)
			journal << commands << std::endl;

		journal.close();
	}

	shutdown_socket(socket);
	shutdown_lib();
}
