#ifndef GAME_STATE_MENU_H
#define GAME_STATE_MENU_H

void GameStateMenuLoad(void);
void GameStateMenuInit(void);
void GameStateMenuUpdate(void);
void GameStateMenuDraw(void);
void GameStateMenuFree(void);
void GameStateMenuUnload(void);
#endif


