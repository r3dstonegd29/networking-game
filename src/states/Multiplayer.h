#ifndef GAME_STATE_MULTIPLAYER_H
#define GAME_STATE_MULTIPLAYER_H

void GameStateMultiplayerLoad(void);
void GameStateMultiplayerInit(void);
void GameStateMultiplayerUpdate(void);
void GameStateMultiplayerDraw(void);
void GameStateMultiplayerFree(void);
void GameStateMultiplayerUnload(void);

#endif