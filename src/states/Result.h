#ifndef GAME_STATE_RESULT_H
#define GAME_STATE_RESULT_H
#include <vector>
void GameStateResultLoad(void);
void GameStateResultInit(void);
void GameStateResultUpdate(void);
void GameStateResultDraw(void);
void GameStateResultFree(void);
void GameStateResultUnload(void);
extern std::vector<unsigned> scoreboard;
extern unsigned myscore;
#endif


