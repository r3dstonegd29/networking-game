#ifndef GAME_STATE_SERVER_H
#define GAME_STATE_SERVER_H

void GameStateServerLoad(void);
void GameStateServerInit(void);
void GameStateServerUpdate(void);
void GameStateServerDraw(void);
void GameStateServerFree(void);
void GameStateServerUnload(void);

#endif