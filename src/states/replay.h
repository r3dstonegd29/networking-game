#ifndef GAME_STATE_REPLAY_H
#define GAME_STATE_REPLAY_H
#include <vector>
void GameStateReplayLoad(void);
void GameStateReplayInit(void);
void GameStateReplayUpdate(void);
void GameStateReplayDraw(void);
void GameStateReplayFree(void);
void GameStateReplayUnload(void);
#endif
