#ifndef GAME_STATE_SINGLEPLAYER_H
#define GAME_STATE_SINGLEPLAYER_H

void GameStateSingleplayerLoad(void);
void GameStateSingleplayerInit(void);
void GameStateSingleplayerUpdate(void);
void GameStateSingleplayerDraw(void);
void GameStateSingleplayerFree(void);
void GameStateSingleplayerUnload(void);

#endif 


