#include <AEEngine.h>
#include "GS_List.h"

static s32 sCursor;

void GameStateMenuLoad(void)
{
}
void GameStateMenuInit(void)
{
	// current selection
	sCursor = 0;
}
void GameStateMenuUpdate(void)
{
	if(AEInputKeyTriggered(VK_UP))
		sCursor--;
	if(AEInputKeyTriggered(VK_DOWN))
		sCursor++;
	const s32 max_cursor = 4;
	sCursor = (sCursor < 0) ? 0 : ((sCursor > max_cursor) ? max_cursor : sCursor);

	if(AEInputKeyTriggered(VK_SPACE))
	{
		switch (sCursor)
		{
		case 0:
			gAEGameStateNext = GS_SINGLEPLAYER;
			break;
		case 1:
			gAEGameStateNext = GS_MULTIPLAYER;
			break;
		case 2:
			gAEGameStateNext = GS_SERVER;
			break;
		case 3:
			gAEGameStateNext = GS_REPLAY;
			break;
		default:
			gAEGameStateNext = GS_QUIT;
		}
	}
}
void GameStateMenuDraw(void)
{
	AEGfxPrint(10, 20, 0xFFFFFFFF, "<> ASTEROID <>");
	AEGfxPrint(40, 60, 0xFFFFFFFF, "Start Game");
	AEGfxPrint(40, 90, 0xFFFFFFFF, "Multiplayer");
	AEGfxPrint(40, 120, 0xFFFFFFFF, "Server");
	AEGfxPrint(40, 150, 0xFFFFFFFF, "Replay");
	AEGfxPrint(40, 180, 0xFFFFFFFF, "Quit");

	if (gAEFrameCounter & 0x0008)
		AEGfxPrint(10, 60 + 30 * sCursor, 0xFFFFFFFF, ">>");
}
void GameStateMenuFree(void)
{
}
void GameStateMenuUnload(void)
{
}