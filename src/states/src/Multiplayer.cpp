#include <AEEngine.h>
#include "GS_List.h"
#include "asteroids.h"
#include "client.h"

Asteroids * multi_game;
Client * client;

void GameStateMultiplayerLoad(void)
{
	client = new Client;
	multi_game = new Asteroids;
}
void GameStateMultiplayerInit(void)
{
	client->Initialize();
	multi_game->Init();
}
void GameStateMultiplayerUpdate(void)
{
	if (client->playing)
	{
		multi_game->Input(client);
		multi_game->Update();
		if (multi_game->check_level_changed())
		{
			gAEGameStateNext = GS_RESULT;
			client->disconnect();
		}
	}
	else
	{
		if (client->connected)
		{
			client->wait_for_start();
			if(client->playing)
				multi_game->create_player(client->client_id), multi_game->client_id = client->client_id;
		}
		else
			client->try_connection();
	}

	if (AEInputKeyPressed(VK_ESCAPE))
		gAEGameStateNext = GS_MENU;
}
void GameStateMultiplayerDraw(void)
{
	if(client->playing)
		multi_game->Draw();
	else
		client->print();
}
void GameStateMultiplayerFree(void)
{
	multi_game->Free();
	client->Shutdown();
}
void GameStateMultiplayerUnload(void)
{

	delete multi_game;
	delete client;
}
