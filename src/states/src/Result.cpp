#include <AEEngine.h>
#include "GS_List.h"
#include "defines.h"
std::vector<unsigned> scoreboard{};
extern unsigned myscore{ 0 };

void GameStateResultLoad(void)
{
}
void GameStateResultInit(void)
{
}
void GameStateResultUpdate(void)
{
	if ((gAEFrameCounter > 60) && (AEInputKeyTriggered(VK_SPACE)))
		gAEGameStateNext = GS_MENU;
}
void GameStateResultDraw(void)
{
	if(myscore < TARGET_SCORE)
		AEGfxPrint(SCR_WIDTH / 2, SCR_HEIGHT / 4, 0xFFFF0000, "GAME OVER");
	else
		AEGfxPrint(SCR_WIDTH / 2, SCR_HEIGHT / 4, 0xFF00FF00, "YOU WIN");
	AEGfxPrint(SCR_WIDTH / 2, SCR_HEIGHT / 4 + 40, 0xFFFFFFFF, "TOTAL SCORE:");

	for (unsigned i = 0; i < scoreboard.size(); i++)
	{
		s8 strBuffer[1024];
		std::string message = "Player " + std::to_string(i) + ": " + std::to_string(scoreboard[i]);
		sprintf(strBuffer, message.c_str());
		AEGfxPrint(SCR_WIDTH / 2, SCR_HEIGHT / 4 + 40 * (i + 2), 0xFFFFFFFF, strBuffer);
	}


	if (gAEFrameCounter > 60)
		AEGfxPrint(SCR_WIDTH / 2, SCR_HEIGHT - 100, 0xFFFFFFFF, "PRESS SPACE TO CONTINUE");
}
void GameStateResultFree(void)
{
	scoreboard = std::vector<unsigned>();
}
void GameStateResultUnload(void)
{
}