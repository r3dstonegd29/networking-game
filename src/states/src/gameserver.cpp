#include <AEEngine.h>
#include "GS_List.h"
#include "server.h"

Server * server;
void GameStateServerLoad(void)
{
	server = new Server;
}

void GameStateServerInit(void)
{
	server->Initialize();
}

void GameStateServerUpdate(void)
{
	server->Update();
	if (AEInputKeyPressed(VK_ESCAPE))
		gAEGameStateNext = GS_MENU;
}

void GameStateServerDraw(void)
{
	//Draw Data
}

void GameStateServerFree(void)
{
	server->Shutdown();
}

void GameStateServerUnload(void)
{
	delete server;
}