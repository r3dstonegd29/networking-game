#include <AEEngine.h>
#include "GS_List.h"
#include "asteroids.h"
#include <fstream>
Asteroids * replay_game;
static std::vector<std::string> cmds;
static unsigned index = 0;
void GameStateReplayLoad(void)
{
	replay_game = new Asteroids;
	cmds.clear();
	index = 0;
}

void GameStateReplayInit(void)
{
	std::ifstream file{ "journal.txt" };
	if (file.is_open())
	{
		std::string str;
		do
		{
			std::getline(file, str);
			if (!str.empty())
				cmds.push_back(str);
		} while (!str.empty());
		replay_game->Init();
		replay_game->create_player(0);
	}
	else
		gAEGameStateNext = GS_MENU;
}

void GameStateReplayUpdate(void)
{
	const unsigned speed = 1;
	std::vector<std::string> current{
		cmds.begin() + index,
		cmds.begin() + index + speed
	};
	index += speed;
	replay_game->process_input(current);
	replay_game->Update();
	if (replay_game->check_level_changed()
	||  index + speed > cmds.size()
	||  AEInputKeyPressed(VK_ESCAPE))
		gAEGameStateNext = GS_MENU;
}

void GameStateReplayDraw(void)
{
	replay_game->Draw();
}

void GameStateReplayFree(void)
{
	replay_game->Free();
}

void GameStateReplayUnload(void)
{
	delete replay_game;
}
