#include <AEEngine.h>
#include "GS_List.h"
#include "asteroids.h"
#include <iostream>

Asteroids * single_game;

void GameStateSingleplayerLoad(void)
{
	single_game = new Asteroids;
}
void GameStateSingleplayerInit(void)
{
	single_game->Init();
	single_game->create_player(0);
}
void GameStateSingleplayerUpdate(void)
{
	single_game->Input(nullptr);
	single_game->Update();
	if (single_game->check_level_changed())
		gAEGameStateNext = GS_RESULT;
	if (AEInputKeyPressed(VK_ESCAPE))
		gAEGameStateNext = GS_MENU;
}
void GameStateSingleplayerDraw(void)
{
	single_game->Draw();
}
void GameStateSingleplayerFree(void)
{
	single_game->Free();
}
void GameStateSingleplayerUnload(void)
{
	delete single_game;
}