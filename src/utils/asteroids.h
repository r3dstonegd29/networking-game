#ifndef ASTEROIDS_H
#define ASTEROIDS_H

#include <AEEngine.h>
#include "defines.h"
#include <array>
#include <vector>
#include <functional>

//-----------------------------------------------------------------------------
class Client;
enum ArchetypeList : u32
{
	//Objects
	ID_SHIP = 0u,
	ID_BULLET,
	ID_BOMB,
	ID_MISSILE,
	ID_ASTEROID,

	// Particles
	ID_PARTICLE_WHITE,
	ID_PARTICLE_YELLOW,
	ID_PARTICLE_RED,

	_count
};

enum class Particle
{
	EXHAUST,
	SMALL,
	MEDIUM,
	LARGE
};

struct Archetype
{
	u32				m_type;		// object type
	AEGfxTriList*	m_mesh;		// pbject
	std::function<void(void)> m_update;
};
 
struct GameObject
{
	Archetype*		m_arch;
	bool			m_alive;
	bool			m_delete;
	f32				m_scale;
	AEVec2			m_position;
	AEVec2			m_velocity;
	f32				m_direction;
	AEMtx33			m_transform;
	f32				m_lifetime;
	void*			m_custom_data;
	u32				m_score;
	GameObject*		m_owner;
	f32				sShipRotSpeed;
	unsigned		m_frame{ 0 };
};

class ArchetypeManager
{
public:
	void initialize();
	void clear();
	GameObject * create_instance(ArchetypeList index, f32 scale, AEVec2 * pPos, AEVec2 * pVel, f32 dir);
private:
	std::array<Archetype, _count> m_archetypes;
};

//-----------------------------------------------------------------------------

class Asteroids
{
	friend struct Archetype;
public:
	Asteroids();
	void Init();
	void Input(Client* client);
	void Update();
	void Draw();
	void Free();
	~Asteroids();
	
	bool check_level_changed();
	std::vector<std::pair<unsigned, GameObject *>> m_ships;
	void create_player(unsigned id);
	GameObject* get_player(unsigned id);
	unsigned client_id{0};
	const unsigned max_frame_delay{ 0 };
	char input{ 0 };
	unsigned targeting{ 0 };
	unsigned frames_till_sync;

private:
	ArchetypeManager m_arch_manager;
	std::vector<GameObject *> m_objects;

	f64				sAstCreationTime;	// keep track when the last asteroid was created
	u32				sAstCtr;			// keep track the total number of asteroid active and the maximum allowed
	u32				sAstNum;
	f64				sGameStateChangeCtr;
	f32				timer = 0;
	s32				sShipCtr;
	s32				sSpecialCtr;
	u32				last_score;

	GameObject*		create_object(ArchetypeList index, f32 scale, AEVec2 * pPos, AEVec2 * pVel, f32 dir);
	void			destroy_object(GameObject * obj);
	void			clear_dead_objects();
	GameObject*		create_asteroid(GameObject* pSrc);
	GameObject*		create_asteroid(float, float, float, float);
	void			create_particle(Particle type, AEVec2* pPos, u32 count, f32 angleMin, f32 angleMax, f32 srcSize = 0.0f, f32 velScale = 1.0f, AEVec2* pVelInit = 0);
	void			resolve_collision(GameObject* pSrc, GameObject* pDst, AEVec2* pNrm);
	GameObject*		missile_target(GameObject* pMissile);
public:
	std::string		get_command();
	void			update_input();
	void			process_input(std::vector<std::string> commands);
private:
	std::vector<float> check_needed_asteroids();
	void			update_all();
	void			update_ship(GameObject * obj);
	void			update_bullet(GameObject * obj);
	void			update_bomb(GameObject * obj);
	void			update_missile(GameObject * obj);
	void			update_asteroid(GameObject * obj);
	void			update_particle(GameObject * obj);
	void			check_collision();

	enum InputKeys : char
	{
		FORWARD = 1 << 0,
		BACKWARD = 1 << 1,
		ROTATE_LEFT = 1 << 2,
		ROTATE_RIGHT = 1 << 3,
		BULLET = 1 << 4,
		BOMB = 1 << 5,
		MISSILE= 1 << 6
	};
};

#endif