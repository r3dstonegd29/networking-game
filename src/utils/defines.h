#ifndef DEFINES_H
#define DEFINES_H

#define SCR_WIDTH 800
#define SCR_HEIGHT 600

#define TARGET_SCORE 50

#define AST_NUM_MIN					5		// minimum number of asteroid alive
#define AST_NUM_MAX					32		// maximum number of asteroid alive
#define AST_SIZE_MAX				200.0f	// maximum asterois size
#define AST_SIZE_MIN				50.0f	// minimum asterois size
#define AST_VEL_MAX					100.0f	// maximum asterois velocity
#define AST_VEL_MIN					50.0f	// minimum asterois velocity
#define AST_CREATE_DELAY			0.1f	// delay of creation between one asteroid to the next
#define AST_SPECIAL_RATIO			4		// number of asteroid for each 'special'
#define AST_SHIP_RATIO				100		// number of asteroid for each ship
#define AST_LIFE_MAX				10		// the life of the biggest asteroid (smaller one is scaled accordingly)
#define AST_VEL_DAMP				1E-8f	// dampening to use if the asteroid velocity is above the maximum
#define AST_TO_SHIP_ACC				0.01f	// how much acceleration to apply to steer the asteroid toward the ship

#define SHIP_INITIAL_NUM			3		// initial number of ship
#define SHIP_SPECIAL_NUM			20
#define SHIP_SIZE					30.0f	// ship size
#define SHIP_ACCEL_FORWARD			100	// ship forward acceleration (in m/s^2)
#define SHIP_ACCEL_BACKWARD			-200.0f	// ship backward acceleration (in m/s^2)
#define SHIP_DAMP_FORWARD			0.55f 	// ship forward dampening
#define SHIP_DAMP_BACKWARD			0.05f 	// ship backward dampening
#define SHIP_ROT_SPEED				(1.0f * PI)	// ship rotation speed (degree/second)

#define BULLET_SPEED				1000.0f	// bullet speed (m/s)
#define BULLET_SIZE					10.0f

#define BOMB_COST					20		// cost to fire a bomb
#define BOMB_RADIUS					250.0f	// bomb radius
#define BOMB_LIFE					5.0f	// how many seconds the bomb will be alive
#define BOMB_SIZE					1.0f

#define MISSILE_COST				1
#define MISSILE_ACCEL				1000.0f		// missile acceleration
#define MISSILE_TURN_SPEED			PI			// missile turning speed (radian/sec)
#define MISSILE_DAMP				1.5E-6f		// missile dampening
#define MISSILE_LIFE				10.0f		// how many seconds the missile will be alive
#define MISSILES_SIZE				10.0f		//	size of missile.

#define PTCL_SCALE_DAMP				0.05f	// particle scale dampening
#define PTCL_VEL_DAMP				0.05f	// particle velocity dampening

#define COLL_COEF_OF_RESTITUTION	1.0f	// collision coefficient of restitution

#endif