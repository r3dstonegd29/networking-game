#include "..\asteroids.h"
#include "Result.h"
#include "client.h"
#include "GS_List.h"
#include <iostream>

//-----------------------------------------------------------------------------

void ArchetypeManager::initialize()
{
	Archetype* pObj;

	// ================
	// create the ship
	// ================

	pObj = &m_archetypes[ID_SHIP];
	pObj->m_type = ID_SHIP;

	AEGfxTriStart();
	AEGfxTriAdd(
		-0.5f, -0.5f, 0xFFFF0000, 0.0f, 0.0f,
		0.5f, 0.0f, 0xFFFFFFFF, 0.0f, 0.0f,
		-0.5f, 0.5f, 0xFFFF0000, 0.0f, 0.0f);

	pObj->m_mesh = AEGfxTriEnd();
	AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");

	// ==================
	// create the bullet
	// ==================

	pObj = &m_archetypes[ID_BULLET];
	pObj->m_type = ID_BULLET;

	AEGfxTriStart();
	AEGfxTriAdd(
		-1.0f, 0.2f, 0x00FFFF00, 0.0f, 0.0f,
		-1.0f, -0.2f, 0x00FFFF00, 0.0f, 0.0f,
		1.0f, -0.2f, 0xFFFFFF00, 0.0f, 0.0f);
	AEGfxTriAdd(
		-1.0f, 0.2f, 0x00FFFF00, 0.0f, 0.0f,
		1.0f, -0.2f, 0xFFFFFF00, 0.0f, 0.0f,
		1.0f, 0.2f, 0xFFFFFF00, 0.0f, 0.0f);

	pObj->m_mesh = AEGfxTriEnd();
	AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");

	// ==================
	// create the bomb
	// ==================

	pObj = &m_archetypes[ID_BOMB];
	pObj->m_type = ID_BOMB;

	AEGfxTriStart();
	AEGfxTriAdd(
		-1.0f, 1.0f, 0xFFFF8000, 0.0f, 0.0f,
		-1.0f, -1.0f, 0xFFFF8000, 0.0f, 0.0f,
		1.0f, -1.0f, 0xFFFF8000, 0.0f, 0.0f);
	AEGfxTriAdd(
		-1.0f, 1.0f, 0xFFFF8000, 0.0f, 0.0f,
		1.0f, -1.0f, 0xFFFF8000, 0.0f, 0.0f,
		1.0f, 1.0f, 0xFFFF8000, 0.0f, 0.0f);

	pObj->m_mesh = AEGfxTriEnd();
	AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");

	// ==================
	// create the missile
	// ==================

	pObj = &m_archetypes[ID_MISSILE];
	pObj->m_type = ID_MISSILE;

	AEGfxTriStart();
	AEGfxTriAdd(
		-1.0f, -0.5f, 0xFFFF0000, 0.0f, 0.0f,
		1.0f, 0.0f, 0xFFFFFF00, 0.0f, 0.0f,
		-1.0f, 0.5f, 0xFFFF0000, 0.0f, 0.0f);

	pObj->m_mesh = AEGfxTriEnd();
	AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");

	// ====================
	// create the asteroid
	// ====================

	pObj = &m_archetypes[ID_ASTEROID];
	pObj->m_type = ID_ASTEROID;

	AEGfxTriStart();
	AEGfxTriAdd(
		-0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f,
		0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f,
		-0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f);
	AEGfxTriAdd(
		-0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f,
		0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f,
		0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f);

	pObj->m_mesh = AEGfxTriEnd();
	AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");

	// ================
	// create the ptcl
	// ================

	for (u32 i = 0; i < 3; i++)
	{
		u32 color = (i == 0) ? (0xFFFFFFFF) : (
			(i == 1) ? (0xFFFFFF00) : (0xFFFF0000));

		pObj = &m_archetypes[ID_PARTICLE_WHITE + i];
		pObj->m_type = ID_PARTICLE_WHITE + i;

		AEGfxTriStart();
		AEGfxTriAdd(
			-1.0f * (3 - i), -0.5f * (3 - i), color, 0.0f, 0.0f,
			1.0f * (3 - i), 0.5f * (3 - i), color, 0.0f, 0.0f,
			-1.0f * (3 - i), 0.5f * (3 - i), color, 0.0f, 0.0f);
		AEGfxTriAdd(
			-1.0f * (3 - i), -0.5f * (3 - i), color, 0.0f, 0.0f,
			1.0f * (3 - i), -0.5f * (3 - i), color, 0.0f, 0.0f,
			1.0f * (3 - i), 0.5f * (3 - i), color, 0.0f, 0.0f);

		pObj->m_mesh = AEGfxTriEnd();
		AE_ASSERT_MESG(pObj->m_mesh != nullptr, "fail to create object!!");
	}
}
void ArchetypeManager::clear()
{
	// free all mesh
	for (u32 u = 0; u < _count; u++)
		AEGfxTriFree(m_archetypes[u].m_mesh);
}
GameObject * ArchetypeManager::create_instance(ArchetypeList index, f32 scale, AEVec2 * pPos, AEVec2 * pVel, f32 dir)
{
	const AEVec2 zero { 0.0f, 0.0f };

	AE_ASSERT(index < _count);

	GameObject* pObj = new GameObject;
	pObj->m_arch = &m_archetypes[index];
	pObj->m_alive = true;
	pObj->m_delete = false;
	pObj->m_lifetime = 1.0f;
	pObj->m_scale = scale;
	pObj->m_position = pPos ? *pPos : zero;
	pObj->m_velocity = pVel ? *pVel : zero;
	pObj->m_direction = dir;
	pObj->m_custom_data = 0;
	pObj->sShipRotSpeed = 0;
	pObj->m_score = 0;
	// return the newly created instance
	return pObj;
}

//-----------------------------------------------------------------------------

Asteroids::Asteroids()
{
	// load/create the mesh data
	m_arch_manager.initialize();
}
void Asteroids::Init()
{
	srand(0);

	// reset the number of current asteroid and the total allowed
	sAstCtr = 0;
	sAstNum = AST_NUM_MIN;

	// get the time the asteroid is created
	sAstCreationTime = AEGetTime();

	// generate the initial asteroid
	for (u32 i = 0; i < sAstNum; i++)
		create_asteroid(0);

	last_score = 0;
	sSpecialCtr = SHIP_SPECIAL_NUM;
	sShipCtr = SHIP_INITIAL_NUM;
	// reset the delay to switch to the result state after game over
	sGameStateChangeCtr = 2.0f;
	targeting = 0;
	frames_till_sync = 0;
}
void Asteroids::Input(Client * client)
{
	GameObject * player = get_player(client ? client->client_id : 0);
	if (player == nullptr) return;
	update_input();
	if (client)
	{
		if (frames_till_sync == 0 || input != 0)
		{
			std::string str = get_command();
			std::vector<std::string> cmds = client->send_recv_input(str);
			for(auto str : cmds)
				process_input(cmds);
			frames_till_sync = max_frame_delay;
			input = 0;
		}
		else
			frames_till_sync--;
	}
	else
		process_input(std::vector<std::string>{get_command()}), input = 0;;
}
void Asteroids::Update()
{
	timer += (f32)(gAEFrameTime);
	update_all();
	check_collision();
	if (auto p = get_player(client_id))
		if(p->m_score >= TARGET_SCORE)
			sShipCtr = -1;
	clear_dead_objects();
}
void Asteroids::Draw()
{
	// =====================================
	// calculate the matrix for all objects
	// =====================================

	for (GameObject * pObj : m_objects)
	{
		AEMtx33		 m;
		AEMtx33Scale(&pObj->m_transform, pObj->m_scale, pObj->m_scale);
		AEMtx33Rot(&m, pObj->m_direction);
		AEMtx33Concat(&pObj->m_transform, &m, &pObj->m_transform);
		AEMtx33Trans(&m, pObj->m_position.x, pObj->m_position.y);
		AEMtx33Concat(&pObj->m_transform, &m, &pObj->m_transform);
	}
	AEGfxReset();

	s8 strBuffer[1024];
	AEMtx33 tmp, tmpScale = AEMtx33::Scale(10, 10);

	// draw all object in the list
	for (GameObject * pObj : m_objects)
	{
		tmp = pObj->m_transform;
		AEGfxSetTransform(&tmp);
		AEGfxTriDraw(pObj->m_arch->m_mesh);

		if(pObj->m_arch->m_type == ID_SHIP)
		{
			sprintf(strBuffer, "Score: %d", pObj->m_score);
			AEGfxPrint(static_cast<s32>(pObj->m_position.x) + SCR_WIDTH/2 - 20, static_cast<s32>(-pObj->m_position.y) - 30 + SCR_HEIGHT/2, -1, strBuffer);
		}
		
	}

	
	sprintf(strBuffer, "Special:   %d", sSpecialCtr);
	AEGfxPrint(600, 20, -1, strBuffer);

	sprintf(strBuffer, "Ship Left: %d", sShipCtr >= 0 ? sShipCtr : 0);
	AEGfxPrint(10, 20, -1, strBuffer);
	sprintf(strBuffer, "Level: %d", AELogBase2(sAstNum));
	AEGfxPrint(10, 30, -1, strBuffer);

	// display the game over message
	if (sShipCtr < 0)
	{
		if(last_score < TARGET_SCORE)
			AEGfxPrint(280, 260, 0xFFFFFFFF, "       GAME OVER       ");
		else
			AEGfxPrint(280, 260, 0xFFFFFFFF, "       YOU WIN         ");
		
	}
}
void Asteroids::Free()
{
	for (GameObject * pObj : m_objects)
		delete pObj;
	m_objects.clear();
}
Asteroids::~Asteroids()
{
	m_arch_manager.clear();
}

bool Asteroids::check_level_changed()
{
	if (get_player(client_id) == nullptr)
	{
		sGameStateChangeCtr -= (f32)(gAEFrameTime);

		if (sGameStateChangeCtr < 0.0)
			return true;
	}

	return false;
}

void Asteroids::create_player(unsigned id)
{
	auto * t = create_object(ID_SHIP, SHIP_SIZE, 0, 0, 0.f);
	m_ships.push_back({ id, t });
}

GameObject * Asteroids::get_player(unsigned id)
{
	for (auto& s : m_ships)
		if (s.first == id)
			return s.second;
	return nullptr;
}

GameObject * Asteroids::create_object(ArchetypeList index, f32 scale, AEVec2 * pPos, AEVec2 * pVel, f32 dir)
{
	GameObject * pObj = m_arch_manager.create_instance(index, scale, pPos, pVel, dir);
	m_objects.push_back(pObj);

	// keep track of asteroids
	if (index == ID_ASTEROID) sAstCtr++;

	return pObj;
}
void Asteroids::destroy_object(GameObject * obj)
{
	if (obj->m_alive)
	{
		obj->m_alive = false;

		// keep track of asteroids
		if (obj->m_arch->m_type == ID_ASTEROID) sAstCtr--;
	}
}
void Asteroids::clear_dead_objects()
{
	for (auto it = m_objects.begin(); it != m_objects.end();)
	{
		if ((*it)->m_delete)
		{
			delete *it;
			it = m_objects.erase(it);
		}
		else
		{
			if (!(*it)->m_alive)
				(*it)->m_delete = true;
			it++;
		}
	}
}
GameObject * Asteroids::create_asteroid(GameObject * pSrc)
{
	GameObject* pInst;
	AEVec2		 pos, vel;
	f32			 t, angle, size;
	if (pSrc)
	{
		f32		posOffset = pSrc->m_scale * 0.25f;
		f32		velOffset = (AST_SIZE_MAX - pSrc->m_scale + 1.0f) * 0.25f;
		f32		scaleNew = pSrc->m_scale * 0.5f;

		create_particle(Particle::LARGE, &pSrc->m_position, 5, 0.0f * PI - 0.01f * PI, 0.0f * PI + 0.01f * PI, 0.0f, pSrc->m_scale / AST_SIZE_MAX, &pSrc->m_velocity);
		create_particle(Particle::LARGE, &pSrc->m_position, 5, 0.5f * PI - 0.01f * PI, 0.5f * PI + 0.01f * PI, 0.0f, pSrc->m_scale / AST_SIZE_MAX, &pSrc->m_velocity);
		create_particle(Particle::LARGE, &pSrc->m_position, 5, 1.0f * PI - 0.01f * PI, 1.0f * PI + 0.01f * PI, 0.0f, pSrc->m_scale / AST_SIZE_MAX, &pSrc->m_velocity);
		create_particle(Particle::LARGE, &pSrc->m_position, 5, 1.5f * PI - 0.01f * PI, 1.5f * PI + 0.01f * PI, 0.0f, pSrc->m_scale / AST_SIZE_MAX, &pSrc->m_velocity);

		pInst = create_asteroid(0);
		pInst->m_scale = scaleNew;
		AEVector2Set(&pInst->m_position, pSrc->m_position.x - posOffset, pSrc->m_position.y - posOffset);
		AEVector2Set(&pInst->m_velocity, pSrc->m_velocity.x - velOffset, pSrc->m_velocity.y - velOffset);

		pInst = create_asteroid(0);
		pInst->m_scale = scaleNew;
		AEVector2Set(&pInst->m_position, pSrc->m_position.x + posOffset, pSrc->m_position.y - posOffset);
		AEVector2Set(&pInst->m_velocity, pSrc->m_velocity.x + velOffset, pSrc->m_velocity.y - velOffset);

		pInst = create_asteroid(0);
		pInst->m_scale = scaleNew;
		AEVector2Set(&pInst->m_position, pSrc->m_position.x - posOffset, pSrc->m_position.y + posOffset);
		AEVector2Set(&pInst->m_velocity, pSrc->m_velocity.x - velOffset, pSrc->m_velocity.y + velOffset);

		pSrc->m_scale = scaleNew;
		AEVector2Set(&pSrc->m_position, pSrc->m_position.x + posOffset, pSrc->m_position.y + posOffset);
		AEVector2Set(&pSrc->m_velocity, pSrc->m_velocity.x + velOffset, pSrc->m_velocity.y + velOffset);

		return pSrc;
	}

	// pick a random angle and velocity magnitude
	angle = AERandFloat() * 2.0f * PI;
	size = AERandFloat() * (AST_SIZE_MAX - AST_SIZE_MIN) + AST_SIZE_MIN;

	// pick a random position along the top or left edge
	if ((t = AERandFloat()) < 0.5f)
		AEVector2Set(&pos, -SCR_WIDTH/2 + (t * 2.0f) * (SCR_WIDTH/2 - -SCR_WIDTH/2), -SCR_HEIGHT/2 - size * 0.5f);
	else
		AEVector2Set(&pos, -SCR_WIDTH/2 - size * 0.5f, -SCR_HEIGHT/2 + ((t - 0.5f) * 2.0f) * (SCR_HEIGHT/2 - -SCR_HEIGHT/2));

	// calculate the velocity vector
	AEVector2Set(&vel, AECos(angle), AESin(angle));
	AEVector2Scale(&vel, &vel, AERandFloat() * (AST_VEL_MAX - AST_VEL_MIN) + AST_VEL_MIN);

	// create the object instance
	pInst = create_object(ID_ASTEROID, size, &pos, &vel, 0.0f);
	AE_ASSERT(pInst != nullptr);

	// set the life based on the size
	pInst->m_lifetime = size / AST_SIZE_MAX * AST_LIFE_MAX;

	return pInst;
}
GameObject * Asteroids::create_asteroid(float r0, float r1, float r2, float r3)
{
	GameObject* pInst;
	AEVec2		 pos, vel;
	f32			 t, angle, size;

	// pick a random angle and velocity magnitude
	angle = r0 * 2.0f * PI;
	size = r1 * (AST_SIZE_MAX - AST_SIZE_MIN) + AST_SIZE_MIN;

	// pick a random position along the top or left edge
	if ((t = r2) < 0.5f)
		AEVector2Set(&pos, -SCR_WIDTH/2 + (t * 2.0f) * (SCR_WIDTH/2 - -SCR_WIDTH/2), -SCR_HEIGHT/2 - size * 0.5f);
	else
		AEVector2Set(&pos, -SCR_WIDTH/2 - size * 0.5f, -SCR_HEIGHT/2 + ((t - 0.5f) * 2.0f) * (SCR_HEIGHT/2 - -SCR_HEIGHT/2));

	// calculate the velocity vector
	AEVector2Set(&vel, AECos(angle), AESin(angle));
	AEVector2Scale(&vel, &vel, r3 * (AST_VEL_MAX - AST_VEL_MIN) + AST_VEL_MIN);

	// create the object instance
	pInst = create_object(ID_ASTEROID, size, &pos, &vel, 0.0f);
	AE_ASSERT(pInst != nullptr);

	// set the life based on the size
	pInst->m_lifetime = size / AST_SIZE_MAX * AST_LIFE_MAX;

	return pInst;
}
void Asteroids::create_particle(Particle type, AEVec2 * pPos, u32 count, f32 angleMin, f32 angleMax, f32 srcSize, f32 velScale, AEVec2 * pVelInit)
{
	f32 velRange, velMin, scaleRange, scaleMin;

	if (type == Particle::EXHAUST)
	{
		velRange = velScale * 30.0f;
		velMin = velScale * 10.0f;
		scaleRange = 5.0f;
		scaleMin = 2.0f;

		for (u32 i = 0; i < count; i++)
		{
			f32		t = AERandFloat() * 2.0f - 1.0f;
			f32		dir = angleMin + AERandFloat() * (angleMax - angleMin);
			f32		velMag = velMin + fabs(t) * velRange;
			AEVec2	vel;

			AEVector2Set(&vel, AECos(dir), AESin(dir));
			AEVector2Scale(&vel, &vel, velMag);

			if (pVelInit)
				AEVector2Add(&vel, &vel, pVelInit);

			create_object(
				(fabs(t) < 0.2f) ? (ID_PARTICLE_YELLOW) : (ID_PARTICLE_RED),
				t * scaleRange + scaleMin,
				pPos, &vel, AERandFloat() * 2.0f * PI);
		}
	}
	else
	{
		if (type == Particle::SMALL)
		{
			velRange = 500.0f;
			velMin = 200.0f;
			scaleRange = 05.0f;
			scaleMin = 02.0f;
		}
		else if (type == Particle::MEDIUM)
		{
			velRange = 1000.0f;
			velMin = 500.0f;
			scaleRange = 05.0f;
			scaleMin = 05.0f;
		}
		else
		{
			velRange = 1500.0f;
			velMin = 200.0f;
			scaleRange = 10.0f;
			scaleMin = 05.0f;
		}

		velRange *= velScale;
		velMin *= velScale;

		for (u32 i = 0; i < count; i++)
		{
			f32		dir = angleMin + (angleMax - angleMin) * AERandFloat();
			f32		t = AERandFloat();
			f32		velMag = t * velRange + velMin;
			AEVec2	vel;
			AEVec2	pos;

			AEVector2Set(&pos, pPos->x + (AERandFloat() - 0.5f) * srcSize, pPos->y + (AERandFloat() - 0.5f) * srcSize);

			AEVector2Set(&vel, AECos(dir), AESin(dir));
			AEVector2Scale(&vel, &vel, velMag);

			if (pVelInit)
				AEVector2Add(&vel, &vel, pVelInit);

			create_object(
				(t < 0.25f) ? (ID_PARTICLE_WHITE) : (
				(t < 0.50f) ? (ID_PARTICLE_YELLOW) : (ID_PARTICLE_RED)),
				t * scaleRange + scaleMin,
				&pos, &vel, AERandFloat() * 2.0f * PI);
		}
	}
}
void Asteroids::resolve_collision(GameObject * pSrc, GameObject * pDst, AEVec2 * pNrm)
{
	f32 ma = pSrc->m_scale * pSrc->m_scale,
		mb = pDst->m_scale * pDst->m_scale,
		e = COLL_COEF_OF_RESTITUTION;

	if (pNrm->y == 0)// EPSILON)
	{
		// calculate the relative velocity of the 1st object againts the 2nd object along the x-axis
		f32 velRel = pSrc->m_velocity.x - pDst->m_velocity.x;

		// if the object is separating, do nothing
		if ((velRel * pNrm->x) >= 0.0f)
			return;

		pSrc->m_velocity.x = (ma * pSrc->m_velocity.x + mb * (pDst->m_velocity.x - e * velRel)) / (ma + mb);
		pDst->m_velocity.x = pSrc->m_velocity.x + e * velRel;
	}
	else
	{
		// calculate the relative velocity of the 1st object againts the 2nd object along the y-axis
		f32 velRel = pSrc->m_velocity.y - pDst->m_velocity.y;

		// if the object is separating, do nothing
		if ((velRel * pNrm->y) >= 0.0f)
			return;

		pSrc->m_velocity.y = (ma * pSrc->m_velocity.y + mb * (pDst->m_velocity.y - e * velRel)) / (ma + mb);
		pDst->m_velocity.y = pSrc->m_velocity.y + e * velRel;
	}
}
GameObject * Asteroids::missile_target(GameObject * pMissile)
{
	AEVec2		 dir, u;
	f32			 uLen, angleMin = AECos(0.25f * PI), minDist = FLT_MAX;
	GameObject* pTarget = nullptr;

	AEVector2Set(&dir, AECos(pMissile->m_direction), AESin(pMissile->m_direction));

	for (GameObject * pObj : m_objects)
	{
		if (!pObj->m_alive || pObj->m_arch->m_type != ID_ASTEROID) continue;
		AEVector2Sub(&u, &pObj->m_position, &pMissile->m_position);
		uLen = AEVector2Length(&u);

		if (uLen < 1.0f)
			continue;

		AEVector2Scale(&u, &u, 1.0f / uLen);

		if (AEVector2DotProduct(&dir, &u) < angleMin)
			continue;

		if (uLen < minDist)
		{
			minDist = uLen;
			pTarget = pObj;
		}
	}

	return pTarget;
}

std::string Asteroids::get_command()
{
	if (sShipCtr < 0)
		return "gameover";
	GameObject * spShip = get_player(client_id);
	std::string cmd{ "P:" + std::to_string(client_id)
		+ '|'
		+ "F:" + std::to_string(spShip->m_frame)
		+ '|'
		+ "pos:" + std::to_string(spShip->m_position.x) + ',' + std::to_string(spShip->m_position.y)
		+ '|'
		+ "vel:" + std::to_string(spShip->m_velocity.x) + ',' + std::to_string(spShip->m_velocity.y)
		+ '|'
		+ "dir:" + std::to_string(spShip->m_direction)
		+ '|'
		+ "avel:" + std::to_string(spShip->sShipRotSpeed)
		+ '|'
		+ "in:" + std::to_string(static_cast<int>(input))
		+ '|' };
	std::vector<float> ast = check_needed_asteroids();
	if (ast.size())
	{
		cmd += std::to_string(ast[0]) + "|";
		cmd += std::to_string(ast[1]) + "|";
		cmd += std::to_string(ast[2]) + "|";
		cmd += std::to_string(ast[3]) + "|";
	}
	return cmd;

}

void Asteroids::update_input()
{
	if (AEInputKeyPressed(VK_UP))	 input |= FORWARD;
	if (AEInputKeyPressed(VK_DOWN))	 input |= BACKWARD;
	if (AEInputKeyPressed(VK_LEFT))	 input |= ROTATE_LEFT;
	if (AEInputKeyPressed(VK_RIGHT)) input |= ROTATE_RIGHT;
	if (AEInputKeyTriggered(VK_SPACE)) input |= BULLET;
	if (AEInputKeyTriggered('Z'))    input |= BOMB;
	if (AEInputKeyTriggered('X'))	 input |= MISSILE;
}

void Asteroids::process_input(std::vector<std::string> commands)
{
	for (auto& c : commands)
	{
		if (c == "gameover")
		{
			sShipCtr = -1;
			sGameStateChangeCtr = 2.0;
			if (scoreboard.empty())
			{
				int i = 0;
				while (true)
				{
					auto player = get_player(i);
					if (player == nullptr)
						break;
					if (i == client_id)myscore = player->m_score;
					scoreboard.push_back(player->m_score);
					i++;
				}
			}

			for (auto& p : m_ships)
				destroy_object(p.second);
			if (auto p = get_player(client_id))
				last_score = p->m_score;
			m_ships.clear();
		}

		if (m_ships.size() == 0)
			break;

		unsigned start, end;
		start = c.find(':') + 1;
		end = c.find('|', start);
		unsigned id = atoi(c.substr(start, end - start).c_str());
		if (get_player(id) == nullptr)create_player(id);

		GameObject * spShip = get_player(id);
		start = c.find(':', end) + 1;
		end = c.find('|', start);
		unsigned frame = atoi(c.substr(start, end - start).c_str());
		bool skip = frame < spShip->m_frame;
		if (!skip)
		{
			spShip->m_frame = frame + 1;
			if((spShip->m_frame % 100) == 0 && id == 0)
				targeting = (targeting + 1) % m_ships.size();
		}
		start = c.find(':', end) + 1;
		end = c.find(',', start);
		if (!skip) spShip->m_position.x = static_cast<f32>(atof(c.substr(start, end-start).c_str()));
		start = end + 1;
		end = c.find('|', start);
		if (!skip) spShip->m_position.y = static_cast<f32>(atof(c.substr(start, end-start).c_str()));
		start = c.find(':', end) + 1;
		end = c.find(',', start);
		if (!skip) spShip->m_velocity.x = static_cast<f32>(atof(c.substr(start, end - start).c_str()));
		start = end + 1;
		end = c.find('|', start);
		if (!skip) spShip->m_velocity.y = static_cast<f32>(atof(c.substr(start, end - start).c_str()));
		start = c.find(':', end) + 1;
		end = c.find('|', start);
		if (!skip) spShip->m_direction = static_cast<f32>(atof(c.substr(start, end - start).c_str()));
		start = c.find(':', end) + 1;
		end = c.find('|', start);
		if (!skip) spShip->sShipRotSpeed = static_cast<f32>(atof(c.substr(start, end - start).c_str()));
		start = c.find(':', end) + 1;
		end = c.find('|', start);
		const char input = atoi(c.substr(start, end - start).c_str());
		c = c.substr(end+1);

		if (c.size()) // NEW ASTEROID
		{
			float v[4];
			end = c.find('|');
			v[0] = static_cast<f32>(atof(c.substr(0, end).c_str()));
			start = end + 1;
			end = c.find('|', start);
			v[1] = static_cast<f32>(atof(c.substr(0, end).c_str()));
			start = end + 1;
			end = c.find('|', start);
			v[2] = static_cast<f32>(atof(c.substr(0, end).c_str()));
			start = end + 1;
			end = c.find('|', start);
			v[3] = static_cast<f32>(atof(c.substr(0, end).c_str()));
			create_asteroid(v[0], v[1], v[2], v[3]);
		}

		if ((input & FORWARD) == FORWARD)
		{
			AEVec2 pos, dir;

			AEVector2Set(&dir, AECos(spShip->m_direction), AESin(spShip->m_direction));
			pos = dir;
			AEVector2Scale(&dir, &dir, SHIP_ACCEL_FORWARD * (f32)(gAEFrameTime));
			AEVector2Add(&spShip->m_velocity, &spShip->m_velocity, &dir);
			AEVector2Scale(&spShip->m_velocity, &spShip->m_velocity, pow(SHIP_DAMP_FORWARD, (f32)(gAEFrameTime)));

			AEVector2Scale(&pos, &pos, -spShip->m_scale);
			AEVector2Add(&pos, &pos, &spShip->m_position);

			create_particle(Particle::EXHAUST, &pos, 2, spShip->m_direction + 0.8f * PI, spShip->m_direction + 1.2f * PI);
		}
		if ((input & BACKWARD) == BACKWARD)
		{
			AEVec2 dir;

			AEVector2Set(&dir, AECos(spShip->m_direction), AESin(spShip->m_direction));
			AEVector2Scale(&dir, &dir, SHIP_ACCEL_BACKWARD * (f32)(gAEFrameTime));
			AEVector2Add(&spShip->m_velocity, &spShip->m_velocity, &dir);
			AEVector2Scale(&spShip->m_velocity, &spShip->m_velocity, pow(SHIP_DAMP_BACKWARD, (f32)(gAEFrameTime)));
		}
		if ((input & ROTATE_LEFT) == ROTATE_LEFT)
		{
			spShip->sShipRotSpeed += (SHIP_ROT_SPEED - spShip->sShipRotSpeed) * 0.1f;
			spShip->m_direction += spShip->sShipRotSpeed * (f32)(gAEFrameTime);
			spShip->m_direction = AEWrap(spShip->m_direction, -PI, PI);
		}
		else if ((input & ROTATE_RIGHT) == ROTATE_RIGHT)
		{
			spShip->sShipRotSpeed += (SHIP_ROT_SPEED - spShip->sShipRotSpeed) * 0.1f;
			spShip->m_direction -= spShip->sShipRotSpeed * (f32)(gAEFrameTime);
			spShip->m_direction = AEWrap(spShip->m_direction, -PI, PI);
		}
		else spShip->sShipRotSpeed = 0.0f;

		if (((input & BULLET) == BULLET) && (timer > 0.05f))
		{
			AEVec2 vel;

			AEVector2Set(&vel, AECos(spShip->m_direction), AESin(spShip->m_direction));
			AEVector2Scale(&vel, &vel, BULLET_SPEED);
			timer = 0;
			create_object(ID_BULLET, BULLET_SIZE, &spShip->m_position, &vel, spShip->m_direction)->m_owner = spShip;
		}
		if (((input & BOMB) == BOMB) && (sSpecialCtr >= BOMB_COST))
		{
			bool active = false;
			// make sure there is no bomb is active currently
			for (GameObject * pObj : m_objects)
				if (pObj->m_arch->m_type == ID_BOMB)
				{
					active = true;
					break;
				}

			// if no bomb is active currently, create one
			if (!active)
			{
				sSpecialCtr -= BOMB_COST;
				create_object(ID_BOMB, BOMB_SIZE, &spShip->m_position, 0, 0)->m_owner = spShip;;
			}
		}
		if (((input & MISSILE) == MISSILE) && (sSpecialCtr >= MISSILE_COST))
		{
			sSpecialCtr -= MISSILE_COST;

			f32			 dir = spShip->m_direction;
			AEVec2       vel = spShip->m_velocity;
			AEVec2		 pos;

			AEVector2Set(&pos, AECos(spShip->m_direction), AESin(spShip->m_direction));
			AEVector2Scale(&pos, &pos, spShip->m_scale * 0.5f);
			AEVector2Add(&pos, &pos, &spShip->m_position);

			create_object(ID_MISSILE, 1.0f, &pos, &vel, dir)->m_owner = spShip;;
		}
	}
}

std::vector<float> Asteroids::check_needed_asteroids()
{
	if (client_id == 0 && (sAstCtr < sAstNum) && ((AEGetTime() - sAstCreationTime) > AST_CREATE_DELAY))
	{
		// keep track the last time an asteroid is created
		sAstCreationTime = AEGetTime();

		// create an asteroid
		return{ AERandFloat(), AERandFloat(), AERandFloat(), AERandFloat() };
	}
	return{};
}
void Asteroids::update_all()
{
	std::vector<GameObject *> current{ m_objects };
	for (GameObject * pObj : current)
	{
		if (!pObj->m_alive) continue;
		pObj->m_position += pObj->m_velocity * (f32)gAEFrameTime;
		switch (pObj->m_arch->m_type)
		{
		case ID_SHIP:
			update_ship(pObj);
			break;
		case ID_BULLET:
			update_bullet(pObj);
			break;
		case ID_BOMB:
			update_bomb(pObj);
			break;
		case ID_MISSILE:
			update_missile(pObj);
			break;
		case ID_ASTEROID:
			update_asteroid(pObj);
			break;
		default:
			update_particle(pObj);
			break;
		}
	}
}
void Asteroids::update_ship(GameObject * obj)
{
	// warp the ship from one end of the screen to the other
	obj->m_position.x = AEWrap(obj->m_position.x, -SCR_WIDTH/2 - SHIP_SIZE, SCR_WIDTH/2 + SHIP_SIZE);
	obj->m_position.y = AEWrap(obj->m_position.y, -SCR_HEIGHT/2 - SHIP_SIZE, SCR_HEIGHT/2 + SHIP_SIZE);
}
void Asteroids::update_bullet(GameObject * obj)
{
	// kill the bullet if it gets out of the screen
	if (!AEInRange(obj->m_position.x, -SCR_WIDTH/2 - AST_SIZE_MAX, SCR_WIDTH/2 + AST_SIZE_MAX) ||
		!AEInRange(obj->m_position.y, -SCR_HEIGHT/2 - AST_SIZE_MAX, SCR_HEIGHT/2 + AST_SIZE_MAX))
		destroy_object(obj);
}
void Asteroids::update_bomb(GameObject * obj)
{
	// adjust the life counter
	obj->m_lifetime -= (f32)(gAEFrameTime) / BOMB_LIFE;

	if (obj->m_lifetime < 0.0f)
	{
		destroy_object(obj);
	}
	else
	{
		f32    radius = 1.0f - obj->m_lifetime;
		AEVec2 u;

		obj->m_direction += 2.0f * PI * (f32)(gAEFrameTime);

		radius = 1.0f - radius;
		radius *= radius;
		radius *= radius;
		radius *= radius;
		radius *= radius;
		radius = (1.0f - radius) * BOMB_RADIUS;

		// generate the particle ring
		for (u32 j = 0; j < 10; j++)
		{
			//f32 dir = AERandFloat() * 2.0f * PI;
			f32 dir = (j / 9.0f) * 2.0f * PI + obj->m_lifetime * 1.5f * 2.0f * PI;

			u.x = AECos(dir) * radius + obj->m_position.x;
			u.y = AESin(dir) * radius + obj->m_position.y;

			//sparkCreate(PTCL_EXHAUST, &u, 1, dir + 0.8f * PI, dir + 0.9f * PI);
			create_particle(Particle::EXHAUST, &u, 1, dir + 0.40f * PI, dir + 0.60f * PI);
		}
	}
}
void Asteroids::update_missile(GameObject * obj)
{
	// adjust the life counter
	obj->m_lifetime -= (f32)(gAEFrameTime) / MISSILE_LIFE;

	if (obj->m_lifetime < 0.0f)
	{
		destroy_object(obj);
	}
	else
	{
		AEVec2 dir;

		if (obj->m_custom_data == nullptr)
		{
			obj->m_custom_data = missile_target(obj);
		}
		else
		{
			GameObject* pTarget = (GameObject*)(obj->m_custom_data);

			// if the target is no longer valid, reacquire
			if (!pTarget->m_alive || pTarget->m_arch->m_type != ID_ASTEROID)
				obj->m_custom_data = missile_target(obj);
		}

		if (obj->m_custom_data)
		{
			GameObject* pTarget = (GameObject*)(obj->m_custom_data);
			AEVec2 u;
			f32    uLen;

			// get the vector from the missile to the target and its length
			AEVector2Sub(&u, &pTarget->m_position, &obj->m_position);
			uLen = AEVector2Length(&u);

			// if the missile is 'close' to target, do nothing
			if (uLen > 0.1f)
			{
				// normalize the vector from the missile to the target
				AEVector2Scale(&u, &u, 1.0f / uLen);

				// calculate the missile direction vector
				AEVector2Set(&dir, AECos(obj->m_direction), AESin(obj->m_direction));

				// calculate the cos and sin of the angle between the target 
				// vector and the missile direction vector
				f32 cosAngle = AEVector2DotProduct(&dir, &u),
					sinAngle = AEVector2CrossProductMag(&dir, &u),
					rotAngle;

				// calculate how much to rotate the missile
				if (cosAngle < AECos(MISSILE_TURN_SPEED * (f32)(gAEFrameTime)))
					rotAngle = MISSILE_TURN_SPEED * (f32)(gAEFrameTime);
				else
					rotAngle = AEACos(AEClamp(cosAngle, -1.0f, 1.0f));

				// rotate to the left if sine of the angle is positive and vice versa
				obj->m_direction += (sinAngle > 0.0f) ? rotAngle : -rotAngle;
			}
		}

		// adjust the missile velocity
		AEVector2Set(&dir, AECos(obj->m_direction), AESin(obj->m_direction));
		AEVector2Scale(&dir, &dir, MISSILE_ACCEL * (f32)(gAEFrameTime));
		AEVector2Add(&obj->m_velocity, &obj->m_velocity, &dir);
		AEVector2Scale(&obj->m_velocity, &obj->m_velocity, pow(MISSILE_DAMP, (f32)(gAEFrameTime)));

		create_particle(Particle::EXHAUST, &obj->m_position, 1, obj->m_direction + 0.8f * PI, obj->m_direction + 1.2f * PI);
	}
}
void Asteroids::update_asteroid(GameObject * obj)
{
	AEVec2		 u;
	f32			 uLen, minDist = FLT_MAX;
	GameObject * pTarget{ nullptr };

	if (pTarget = get_player(targeting))
	{
		// apply acceleration propotional to the distance from the asteroid to the ship
		AEVector2Sub(&u, &pTarget->m_position, &obj->m_position);
		AEVector2Scale(&u, &u, AST_TO_SHIP_ACC * (f32)(gAEFrameTime));
		AEVector2Add(&obj->m_velocity, &obj->m_velocity, &u);

		// if the asterid velocity is more than its maximum velocity, reduce its speed
		if ((uLen = AEVector2Length(&obj->m_velocity)) > (AST_VEL_MAX * 2.0f))
		{
			AEVector2Scale(&u, &obj->m_velocity, (1.0f / uLen) * (AST_VEL_MAX * 2.0f - uLen) * pow(AST_VEL_DAMP, (f32)(gAEFrameTime)));
			AEVector2Add(&obj->m_velocity, &obj->m_velocity, &u);
		}
	}
}
void Asteroids::update_particle(GameObject * obj)
{
	obj->m_scale *= pow(PTCL_SCALE_DAMP, (f32)(gAEFrameTime));
	obj->m_direction += 0.1f;
	AEVector2Scale(&obj->m_velocity, &obj->m_velocity, pow(PTCL_VEL_DAMP, (f32)(gAEFrameTime)));

	if (obj->m_scale < PTCL_SCALE_DAMP)
		destroy_object(obj);
}
void Asteroids::check_collision()
{
	std::vector<GameObject *> current{ m_objects };
	for (GameObject * pSrc : current)
	{
		if (!pSrc->m_alive) continue;
		if ((pSrc->m_arch->m_type == ID_BULLET) || (pSrc->m_arch->m_type == ID_MISSILE))
		{
			for (GameObject * pDst : current)
			{
				if (!pSrc->m_alive || pDst->m_arch->m_type != ID_ASTEROID)
					continue;

				if (AEStaticPointToStaticRect(&pSrc->m_position, &pDst->m_position, pDst->m_scale, pDst->m_scale) == false)
					continue;

				if (pDst->m_scale < AST_SIZE_MIN)
				{
					create_particle(Particle::MEDIUM, &pDst->m_position, (u32)(pDst->m_scale * 10), pSrc->m_direction - 0.05f * PI, pSrc->m_direction + 0.05f * PI, pDst->m_scale);
					pSrc->m_owner->m_score++;

					if ((pSrc->m_owner->m_score % AST_SPECIAL_RATIO) == 0)
						sSpecialCtr++;
					if ((pSrc->m_owner->m_score % AST_SHIP_RATIO) == 0)
						sShipCtr++;
					if (pSrc->m_owner->m_score == sAstNum * 5)
						sAstNum = (sAstNum < AST_NUM_MAX) ? (sAstNum * 2) : sAstNum;

					// destroy the asteroid
					destroy_object(pDst);
				}
				else
				{
					create_particle(Particle::SMALL, &pSrc->m_position, 10, pSrc->m_direction + 0.9f * PI, pSrc->m_direction + 1.1f * PI);

					// impart some of the bullet/missile velocity to the asteroid
					AEVector2Scale(&pSrc->m_velocity, &pSrc->m_velocity, 0.01f * (1.0f - pDst->m_scale / AST_SIZE_MAX));
					AEVector2Add(&pDst->m_velocity, &pDst->m_velocity, &pSrc->m_velocity);

					// split the asteroid to 4
					if ((pSrc->m_arch->m_type == ID_MISSILE) || ((pDst->m_lifetime -= 1.0f) < 0.0f))
						create_asteroid(pDst);
				}

				// destroy the bullet
				destroy_object(pSrc);

				break;
			}
		}
		else if (ID_BOMB == pSrc->m_arch->m_type)
		{
			f32 radius = 1.0f - pSrc->m_lifetime;

			pSrc->m_direction += 2.0f * PI * (f32)(gAEFrameTime);

			radius = 1.0f - radius;
			radius *= radius;
			radius *= radius;
			radius *= radius;
			radius *= radius;
			radius *= radius;
			radius = (1.0f - radius) * BOMB_RADIUS;

			// check collision
			for (GameObject * pDst : current)
			{

				if (!pSrc->m_alive || pDst->m_arch->m_type != ID_ASTEROID)
					continue;

				//if (AECalcDistPointToRect(&pSrc->posCurr, &pDst->posCurr, pDst->scale, pDst->scale) > radius)
				if (AEStaticPointToStaticCircle(&pSrc->m_position, &pDst->m_position, radius) == false)
					continue;

				if (pDst->m_scale < AST_SIZE_MIN)
				{
					f32 dir = atan2f(pDst->m_position.y - pSrc->m_position.y, pDst->m_position.x - pSrc->m_position.x);

					destroy_object(pDst);
					create_particle(Particle::MEDIUM, &pDst->m_position, 20, dir + 0.4f * PI, dir + 0.45f * PI);
					pSrc->m_owner->m_score++;

					if ((pSrc->m_owner->m_score % AST_SPECIAL_RATIO) == 0)
						sSpecialCtr++;
					if ((pSrc->m_owner->m_score % AST_SHIP_RATIO) == 0)
						sShipCtr++;
					if (pSrc->m_owner->m_score == sAstNum * 5)
						sAstNum = (sAstNum < AST_NUM_MAX) ? (sAstNum * 2) : sAstNum;
				}
				else
				{
					// split the asteroid to 4
					create_asteroid(pDst);
				}
			}
		}
		else if (pSrc->m_arch->m_type == ID_ASTEROID)
		{
			for (GameObject * pDst : current)
			{
				f32          d;
				AEVec2       nrm, u;

				// skip no-active and non-asteroid object
				if (!pSrc->m_alive || pDst->m_arch->m_type != ID_ASTEROID)
					continue;

				// check if the object rectangle overlap
				d = AEStaticRectToStaticRect(
					&pSrc->m_position, pSrc->m_scale, pSrc->m_scale,
					&pDst->m_position, pDst->m_scale, pDst->m_scale);
				//&nrm);

				if (d >= 0.0f)
					continue;

				// adjust object position so that they do not overlap
				AEVector2Scale(&u, &nrm, d * 0.25f);
				AEVector2Sub(&pSrc->m_position, &pSrc->m_position, &u);
				AEVector2Add(&pDst->m_position, &pDst->m_position, &u);

				// calculate new object velocities
				resolve_collision(pSrc, pDst, &nrm);
			}
		}
		else if (pSrc->m_arch->m_type == ID_SHIP)
		{
			for (GameObject * pDst : current)
			{

				// skip no-active and non-asteroid object
				if (!pSrc->m_alive || (pSrc == pDst) || (pDst->m_arch->m_type != ID_ASTEROID))
					continue;

				// check if the object rectangle overlap
				if (AEStaticRectToStaticRect(
					&pSrc->m_position, pSrc->m_scale, pSrc->m_scale,
					&pDst->m_position, pDst->m_scale, pDst->m_scale) == false)
					continue;

				// create the big explosion
				create_particle(Particle::LARGE, &pSrc->m_position, 100, 0.0f, 2.0f * PI);

				// reset the ship position and direction
				AEVector2Zero(&get_player(client_id)->m_position);
				AEVector2Zero(&get_player(client_id)->m_velocity);
				get_player(client_id)->m_direction = 0.0f;

				sSpecialCtr = SHIP_SPECIAL_NUM;

				// destroy all asteroid near the ship so that you do not die as soon as the ship reappear
				for (GameObject * pInst : current)
				{
					AEVec2		 u;
					if (!pSrc->m_alive || pInst->m_arch->m_type != ID_ASTEROID)
						continue;

					AEVector2Sub(&u, &pInst->m_position, &get_player(client_id)->m_position);

					if (AEVector2Length(&u) < (get_player(client_id)->m_scale * 10.0f))
					{
						create_particle(Particle::MEDIUM, &pInst->m_position, 10, -PI, PI);
						destroy_object(pInst);
					}
				}

				// reduce the ship counter
				sShipCtr--;
				break;
			}
		}
	}

}